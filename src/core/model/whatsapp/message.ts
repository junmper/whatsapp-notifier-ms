export interface Message {
    id?: any;
    message: string;
    phone?: string;
    error?: any;
}